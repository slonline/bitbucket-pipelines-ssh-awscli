FROM atlassian/default-image:2
MAINTAINER SLONline, s.r.o. - Lubos Odraska

RUN	apt-get update \
	&& apt-get install -y \
		openssh-client \
        	python-pip

RUN	pip install awscli --upgrade

ENTRYPOINT /bin/bash
